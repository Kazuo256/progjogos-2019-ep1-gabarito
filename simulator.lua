
local FIGHT = require 'fight'
local STATE = require 'state'

local SIMULATOR = {}

function SIMULATOR.run(scenario)
  math.randomseed(scenario.seed)
  local state = STATE(scenario)
  for _, fight in ipairs(state.fights) do
    FIGHT(state, unpack(fight))
  end
  return state.units
end

return SIMULATOR

