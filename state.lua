
local TRIANGLE  = require 'triangle'
local MAGIC     = require 'magic'

local STATE = {}

STATE.__index = STATE

local function new(state)
  return setmetatable(state, STATE)
end

function STATE:__call(name)
  return self.units[name] or self.weapons[name]
end

function STATE:usedAttributes(attacker)
  if MAGIC[self(self(attacker).weapon).kind] then
    return 'mag', 'res'
  else
    return 'str', 'def'
  end
end

function STATE:triangleAdvantage(attacker, defender)
  local atkwpn = self(self(attacker).weapon)
  local defwpn = self(self(defender).weapon)
  return TRIANGLE[atkwpn.kind] == defwpn.kind and 1 or 0
end

function STATE:damage(attacker, defender)
  local atk_attr, def_attr = self:usedAttributes(attacker)
  local atkunit = self(attacker)
  local defunit = self(defender)
  local weapon = self(atkunit.weapon)
  local bonus = self:triangleAdvantage(attacker, defender)
  local eff = (weapon.eff and weapon.eff == defunit.trait) and 2 or 1
  local damage = atkunit[atk_attr] + (weapon.mt + bonus) * eff
  local defense = defunit[def_attr]
  return math.max(0, damage - defense)
end

function STATE:attackSpeed(name)
  local unit = self(name)
  local wpn_wt = self(unit.weapon).wt
  return unit.spd - math.max(0, wpn_wt - unit.str)
end

function STATE:accuracy(attacker, defender)
  local unit = self(attacker)
  local weapon = self(unit.weapon)
  local bonus = self:triangleAdvantage(attacker, defender) * 10
              - self:triangleAdvantage(defender, attacker) * 10
  return weapon.hit + unit.skl * 2 + unit.lck + bonus
end

function STATE:avoid(name)
  local atkspd = self:attackSpeed(name)
  local unit = self(name)
  return atkspd * 2 + unit.lck
end

function STATE:hitChance(attacker, defender)
  local acc = self:accuracy(attacker, defender)
  local avo = self:avoid(defender)
  return math.max(0, math.min(100, acc - avo))
end

function STATE:criticalRate(name)
  local unit = self(name)
  local weapon = self(unit.weapon)
  return weapon.crt + math.floor(unit.skl / 2)
end

function STATE:dodge(name)
  return self(name).lck
end

function STATE:battleCritical(attacker, defender)
  return math.max(0, self:criticalRate(attacker) - self:dodge(defender))
end

return new

