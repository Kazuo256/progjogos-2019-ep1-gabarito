
local function _critical(state, attacker, defender)
  local crit_rate = state:battleCritical(attacker, defender)
  local roll = math.random(100)
  if roll <= crit_rate then
    print(("It's a critical! (%d)"):format(roll))
    return 3
  else
    return 1
  end
end

local function _dealDamage(state, attacker, defender)
  local damage = state:damage(attacker, defender)
  damage = damage * _critical(state, attacker, defender)
  local new_hp = math.max(0, state(defender).hp - damage)
  print(("%s takes %d damage and their HP is now %d"):format(defender, damage,
                                                             new_hp))
  state(defender).hp = new_hp
end

local function _strike(state, attacker, defender)
  print(("%s strikes %s"):format(attacker, defender))
  local hit_chance = state:hitChance(attacker, defender)
  local r1, r2 = math.random(100), math.random(100)
  local roll = (r1 + r2) / 2
  if roll <= hit_chance then
    print(("They hit! (%d/2 + %d/2 = %d)"):format(r1, r2, roll))
    _dealDamage(state, attacker, defender)
  else
    print(("They miss! (%d/2 + %d/2 = %d)"):format(r1, r2, roll))
  end
end

local function _preview(state, attacker, defender)
  local atkspd = state:attackSpeed(attacker)
  local defspd = state:attackSpeed(defender)
  print(("\nFight %9s  %9s"):format(attacker, defender))
  print("--------------------------")
  print(("HP    %9d  %9d"):format(state(attacker).hp, state(defender).hp))
  print(("Hit   %9d  %9d"):format(state:hitChance(attacker, defender),
                                  state:hitChance(defender, attacker)))
  print(("Dmg   %9d%s%9d%s"):format(state:damage(attacker, defender),
                                      atkspd >= defspd + 4 and 'x2' or '  ',
                                      state:damage(defender, attacker),
                                      defspd >= atkspd + 4 and 'x2' or ''))
  print(("Crt   %9d  %9d"):format(state:battleCritical(attacker, defender),
                                  state:battleCritical(defender, attacker)))
  print("")
end

local function _fight(state, attacker, defender)
  _preview(state, attacker, defender)
  local atkspd = state:attackSpeed(attacker)
  local defspd = state:attackSpeed(defender)
  _strike(state, attacker, defender)
  if state(defender).hp > 0 then
    _strike(state, defender, attacker)
    if state(attacker).hp > 0 then
      if atkspd >= defspd + 4 then
        _strike(state, attacker, defender)
      elseif defspd >= atkspd + 4 then
        _strike(state, defender, attacker)
      end
    end
  end
end

return _fight

